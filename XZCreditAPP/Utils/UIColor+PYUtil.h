//
//  UIColor+PYUtil.h
//  Pods
//
//  Created by yunhe.lin on 16/7/27.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (PYUtil)

+ (UIColor *)py_colorWithHex:(NSInteger)hex;

+ (UIColor *)py_colorWithHex:(NSInteger)hex colorAlpha:(CGFloat)alpha;

@end
