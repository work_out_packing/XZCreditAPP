//
//  NSString+pyUtil.m
//  Pods
//
//  Created by yunhe.lin on 16/7/26.
//
//

#import "NSString+pyUtil.h"

@implementation NSString (PYUtil)

- (CGSize)py_sizeForFont:(UIFont *)font contentSize:(CGSize)size
{
    CGSize titleSize = [self boundingRectWithSize:size
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil].size;
    return titleSize;
}
- (CGSize)py_sizeForFont:(UIFont *)font
{
    return [self sizeWithFont:font];
}

- (CGSize)py_sizeForFont:(UIFont *)font contentSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    NSDictionary *attributes = @{NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName:paragraphStyle.copy
                                 };
    return [self boundingRectWithSize:size
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:attributes
                              context:nil].size;
}

- (BOOL)py_hasContainStr:(NSString *)str
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        return [self containsString:str];
    }
    return [self componentsSeparatedByString:str].count > 0;
}

- (NSAttributedString *)py_attributedStringForLineSpace:(CGFloat)lineSpace strFont:(UIFont *)font
{
    NSMutableParagraphStyle *pragraphStyle = [[NSMutableParagraphStyle alloc] init];
    pragraphStyle.lineSpacing = lineSpace;
    NSDictionary *atsDict = @{
                              NSFontAttributeName : font,
                              NSParagraphStyleAttributeName : pragraphStyle,
                              };
    return [[NSAttributedString alloc] initWithString:self attributes:atsDict];
}

- (NSInteger)py_length
{
    NSInteger asciiLength = 0;
    for (NSInteger i = 0; i < self.length; i++)
    {
        unichar uc = [self characterAtIndex: i];
        asciiLength += isascii(uc) ? 1 : 2;
    }
    return asciiLength;
}

- (NSString *)py_transform:(NSString *)chinese
{
    //将NSString装换成NSMutableString
    NSMutableString *pinyin = [chinese mutableCopy];
    //将汉字转换为拼音(带音标)
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    NSLog(@"%@", pinyin);
    //去掉拼音的音标
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    NSLog(@"%@", pinyin);
    //返回最近结果
    return pinyin;
}

+ (NSString *)stringWithDate:(NSDate *)firstDate second:(NSDate *)lastDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:firstDate];
    NSString *strDate1 = [dateFormatter stringFromDate:lastDate];
    return [NSString stringWithFormat:@"%@|%@", strDate, strDate1];
}

@end
