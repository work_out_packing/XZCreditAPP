//
//  AppDelegate.h
//  XZCreditAPP
//
//  Created by yunhe.lin on 2017/6/12.
//  Copyright © 2017年 XZDMW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

