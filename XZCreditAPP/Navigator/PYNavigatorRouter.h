//
//  PYNavigatorRouter.h
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import <Foundation/Foundation.h>
#import "PYNavigatorRouterProtocol.h"

@interface PYNavigatorRouter : NSObject<PYNavigatorRouterProtocol>

+ (instancetype)instance;

@end
