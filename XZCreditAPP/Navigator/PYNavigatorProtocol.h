//
//  PYNavigatorProtocol.h
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import <Foundation/Foundation.h>

@protocol PYNavigatorProtocol <NSObject>

- (void)setRootViewController:(UIViewController *)vc;

- (void)pushViewController:(NSDictionary *)dict animate:(BOOL)animated;

- (void)presentViewController:(NSDictionary *)dict animate:(BOOL)animated;

- (void)popViewController:(BOOL)animate;

- (void)pushViewController:(UIViewController *)viewController;
@end

@protocol UIViewControllerNavigatorProtocol <NSObject>

- (void)setParam:(NSDictionary *)param;

@end
