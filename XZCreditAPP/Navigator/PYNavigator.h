//
//  PYNavigator.h
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import <Foundation/Foundation.h>
#import "PYNavigatorProtocol.h"

@interface MainModel : NSObject

+ (void)setFlagViewHidden:(BOOL)hidden;

@end

@interface PYNavigator : NSObject<PYNavigatorProtocol>

+ (instancetype)instance;

- (UINavigationController *)rootNav;

- (void)setLastRootNav:(UINavigationController *)nav;

@end
