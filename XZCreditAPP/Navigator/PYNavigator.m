//
//  PYNavigator.m
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import "PYNavigator.h"
#import "PYNavigatorRouter.h"

static PYNavigator *navigator = nil;
static NSString *const kTarget = @"target";

@interface PYNavigator()

@property (nonatomic, strong) UINavigationController *rootNav;

@end

@implementation PYNavigator

+ (instancetype)instance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        navigator = [[self alloc] init];
    });
    return navigator;
}

- (UINavigationController *)rootNav
{
    return _rootNav;
}

- (void)setLastRootNav:(UINavigationController *)nav
{
    _rootNav = nav;
}

- (void)setRootViewController:(UIViewController *)vc
{
    if (!self.rootNav) {
        self.rootNav = [[UINavigationController alloc] initWithRootViewController:vc];
    }
    [self.rootNav setViewControllers:@[vc]];
}

- (instancetype)init
{
    if (self = [super init]) {
        [[PYNavigatorRouter instance] registerUrl:@"py/navigator/pop" forAction:^id(id param) {
            NSArray *viewControllers = self.rootNav.viewControllers;
            NSInteger vcCount = self.rootNav.viewControllers.count;
            UIViewController *paramVC = param[@"vc"];
            __block NSInteger index = 0;
            [viewControllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isEqual:paramVC]) {
                    index = idx;
                    *stop = YES;
                }
            }];
            if (index == vcCount - 1) { // 栈顶vc pop
                [self.rootNav popViewControllerAnimated:YES];
            } else if (index > 0 && index < vcCount -1) {
                __block NSMutableArray *vcs = [[NSMutableArray alloc] init];
                [viewControllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (idx <= index) {
                        [vcs addObject:obj];
                    }
                }];
                [self.rootNav setViewControllers:vcs];
            }
            return @0;
        }];
    }
    return self;
}

- (id)viewControllerForDict:(NSDictionary *)dict
{
    id vc = [[PYNavigatorRouter instance] viewControllerOf:dict[kTarget]];
    if (!vc) {
        NSString *log = [NSString stringWithFormat:@"target: %@ 没有注册对应的vc 无法获取",dict[kTarget]];
        NSAssert(vc == nil, log);
    }
    if ([vc respondsToSelector:@selector(setParam:)]) {
//        [vc performSelector:@selector(setParam:) withObject:dict afterDelay:0.01];
        [vc performSelector:@selector(setParam:) withObject:dict];
    } else {
#if DEBUG
        NSLog(@"不存在setParam方法");
#endif
    }
    return vc;
}

- (void)pushViewController:(NSDictionary *)dict animate:(BOOL)animated
{
    id vc = [self viewControllerForDict:dict];
    if (!vc) {
        return;
    }
    
    [self.rootNav pushViewController:vc animated:animated];
}

- (void)pushViewController:(UIViewController *)viewController
{
    [self.rootNav pushViewController:viewController animated:YES];
}

- (void)presentViewController:(NSDictionary *)dict animate:(BOOL)animated
{
    id vc = [self viewControllerForDict:dict];
    if (!vc) {
        return;
    }
    [self.rootNav presentViewController:vc animated:animated completion:nil];
}

- (void)popViewController:(BOOL)animate
{
    [self.rootNav popViewControllerAnimated:animate];
}


@end

