//
//  PYNavigatorRouterProtocol.h
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import <Foundation/Foundation.h>

typedef id(^RegisterAction)(id param);

@protocol PYNavigatorRouterProtocol <NSObject>

- (void)registerUrl:(NSString *)url forVCClass:(Class)vCClass;

- (void)registerUrl:(NSString *)url forAction:(RegisterAction)action;

- (UIViewController *)viewControllerOf:(NSString *)target;

- (void)actionFor:(NSString *)url actionParam:(id)param;

@end
