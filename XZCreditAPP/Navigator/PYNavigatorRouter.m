//
//  PYNavigatorRouter.m
//  Pods
//
//  Created by yunhe.lin on 16/10/13.
//
//

#import "PYNavigatorRouter.h"

static NSString *const kActionAppend = @"~";
static PYNavigatorRouter *navigatorRouter = nil;

@interface PYNavigatorRouter()

@property (nonatomic, strong) NSMutableDictionary *routers;

@end

@implementation PYNavigatorRouter

+ (instancetype)instance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        navigatorRouter = [[self alloc] init];
    });
    return navigatorRouter;
}

- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark -- PYNavigatorRouterProtocol

- (void)registerUrl:(NSString *)url forVCClass:(Class)vCClass
{
    if (url.length <= 0) {
        return;
    }
    if (!vCClass) {
#if DEBUG
        NSLog(@"target -- > %@  vcClass: %@", url, vCClass);
#endif
        return;
    }
    self.routers[url] = vCClass;
}

- (void)registerUrl:(NSString *)url forAction:(RegisterAction)action
{
    if (url.length == 0 || action == nil) {
        return;
    }
    NSString *appendUrl = [NSString stringWithFormat:@"%@-%@",url, kActionAppend];
    self.routers[appendUrl] = [action copy];
}

- (UIViewController *)viewControllerOf:(NSString *)target
{
    Class vcClass = self.routers[target];
    if (!vcClass) {
#if DEBUG
        NSLog(@"target -- > %@  vcClass: %@", target, vcClass);
#endif
        return nil;
    }
    return (UIViewController *)[[vcClass alloc] init];
}

- (void)actionFor:(NSString *)url actionParam:(id)param
{
    if (url.length == 0) {
        return;
    }
    NSString *appendUrl = [NSString stringWithFormat:@"%@-%@",url, kActionAppend];
    RegisterAction action = self.routers[appendUrl];
    if (action) {
        action(param);
    }
}

#pragma mark - accessors method 

- (NSMutableDictionary *)routers
{
	if(_routers == nil) {
		_routers = [[NSMutableDictionary alloc] init];
	}
	return _routers;
}

@end
