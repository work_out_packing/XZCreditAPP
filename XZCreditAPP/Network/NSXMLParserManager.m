//
//  NSXMLParserManager.m
//  YNBao
//
//  Created by yunhe.lin on 16/12/13.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "NSXMLParserManager.h"


@interface NSXMLParserManager()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableData *data;

@property (nonatomic, strong) NSMutableArray *arr;

@property (nonatomic, strong) NSMutableDictionary *tempDic;

@property (nonatomic, strong) NSString *tempString1;
@property (nonatomic, strong) NSString *tempString2;
@property (nonatomic, strong) NSString *tempString3;

@property (nonatomic, copy) ParserBlock parserBlock;

@end

@implementation NSXMLParserManager

- (void)parserData:(NSXMLParser *)parser parserBlock:(ParserBlock)block
{
    self.parserBlock = block;
    NSDictionary *dict = [[XMLDictionaryParser sharedInstance] dictionaryWithParser:parser];
    if (block) {
        block(dict);
    }
}

@end
