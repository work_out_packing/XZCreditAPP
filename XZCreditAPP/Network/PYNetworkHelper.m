//
//  PYNetworkHelper.m
//  Pods
//
//  Created by yunhe.lin on 16/10/11.
//
//

#import "PYNetworkHelper.h"
#import "HYBNetworking.h"

static PYNetworkHelper *instance = nil;
static NSString *baseUrl = @"http://58.213.139.250:9093/interface.asmx/";

@implementation PYNetworkHelper

+ (instancetype)instance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PYNetworkHelper alloc] init];
    });
    return instance;
}

+ (void)updateBaseUrl:(NSString *)url
{
    baseUrl = url;
    [HYBNetworking updateBaseUrl:url];
}

+ (NSString *)baseUrl
{
    return baseUrl;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self helperConfig];
    }
    return self;
}

- (void)helperConfig
{
    [HYBNetworking enableInterfaceDebug:true];
    [HYBNetworking setTimeout:30.f];
    [HYBNetworking obtainDataFromLocalWhenNetworkUnconnected:YES];
    [HYBNetworking configRequestType:kHYBRequestTypePlainText responseType:kHYBResponseTypeJSON shouldAutoEncodeUrl:false callbackOnCancelRequest:true];
}

- (NSURLSessionTask *)getWithUrl:(NSString *)url
                    refreshCache:(BOOL)refreshCache
                          params:(NSDictionary *)params
                         success:(PYonCompleteBlock)onComplete
                            fail:(PYonErrorBlock)onError
                       modelName:(NSString *)modelName
{
    @weakify(self);
    HYBURLSessionTask *task = [HYBNetworking getWithUrl:url refreshCache:refreshCache params:params success:^(id response) {
        @strongify(self);
        [self successRequest:response modelName:modelName onComplete:onComplete error:onError];
    } fail:^(NSError *error) {
        BLOCK_EXEC(onError,error);
    }];
    return task;
}

- (NSURLSessionTask *)postWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(PYonCompleteBlock)onComplete
                             fail:(PYonErrorBlock)onError
                        modelName:(NSString *)modelName
{
    @weakify(self);
    return [HYBNetworking postWithUrl:url refreshCache:refreshCache params:params success:^(id response) {
        @strongify(self);
        [self successRequest:response modelName:modelName onComplete:onComplete error:onError];
    } fail:^(NSError *error) {
        BLOCK_EXEC(onError,error);
    }];
}

- (void)successRequest:(id)result modelName:(NSString *)modelName onComplete:(PYonCompleteBlock)onComplete error:(PYonErrorBlock)errorBlock
{
    // 转化model
}



@end
