//
//  NSXMLParserManager.h
//  YNBao
//
//  Created by yunhe.lin on 16/12/13.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ParserBlock)(id);

@interface NSXMLParserManager : NSObject

- (void)parserData:(NSData *)data parserBlock:(ParserBlock)block;

@end
