//
//  PYNetworkHelper.h
//  Pods
//
//  Created by yunhe.lin on 16/10/11.
//
//

#import <Foundation/Foundation.h>
#import "PYNetworkHeader.h"

@interface PYNetworkHelper : NSObject

+ (instancetype)instance;

+ (void)updateBaseUrl:(NSString *)url;

+ (NSString *)baseUrl;

- (NSURLSessionTask *)getWithUrl:(NSString *)url
                    refreshCache:(BOOL)refreshCache
                          params:(NSDictionary *)params
                         success:(PYonCompleteBlock)onComplete
                            fail:(PYonErrorBlock)onError
                       modelName:(NSString *)modelName;

- (NSURLSessionTask *)postWithUrl:(NSString *)url
                     refreshCache:(BOOL)refreshCache
                           params:(NSDictionary *)params
                          success:(PYonCompleteBlock)onComplete
                             fail:(PYonErrorBlock)onError
                        modelName:(NSString *)modelName;

@end
