//
//  PYNetworkHeader.h
//  Pods
//
//  Created by yunhe.lin on 16/10/11.
//
//

#ifndef PYNetworkHeader_h
#define PYNetworkHeader_h

#import "MJExtension.h"
#import "EXTScope.h"

#pragma mark - network使用
typedef void(^PYonCompleteBlock)(id result);
typedef void(^PYonErrorBlock)(id error);

#define BLOCK_EXEC(block, ...) if (block) { block(__VA_ARGS__); };

#endif /* PYNetworkHeader_h */
