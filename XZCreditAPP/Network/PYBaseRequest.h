//
//  PYBaseRequest.h
//  Pods
//
//  Created by yunhe.lin on 16/10/11.
//
//

#import <Foundation/Foundation.h>
#import "PYNetworkHeader.h"

typedef NS_ENUM(NSInteger, PYHttpType) {
    PYHttpTypePOST,
    PYHttpTypeGET
};

@interface PYBaseRequest : NSObject

@property (nonatomic, strong) NSMutableDictionary *requestParam;
@property (nonatomic, copy)   PYonCompleteBlock completeBlock;
@property (nonatomic, copy)   PYonErrorBlock    errorBlock;
@property (nonatomic, copy)   NSString *methodName;
@property (nonatomic, assign) PYHttpType httpType;
@property (nonatomic, copy)   NSString *url;

- (void)sendRequest;

- (NSString *)modelName;

@end
