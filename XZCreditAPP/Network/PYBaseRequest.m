//
//  PYBaseRequest.m
//  Pods
//
//  Created by yunhe.lin on 16/10/11.
//
//

#import "PYBaseRequest.h"
#import "PYNetworkHelper.h"

typedef void(^RequestBlock)(void);

@implementation PYBaseRequest

- (instancetype)init
{
    if (self = [super init]) {
        self.requestParam = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return self;
}

- (NSString *)url
{
    return [NSString stringWithFormat:@"%@%@",[PYNetworkHelper baseUrl],[self methodName]];
}

- (NSString *)modelName
{
    return @"";
}

- (void)sendRequest
{
    RequestBlock requestBlock = ^(void) {
        NSString *url = [self url];
        if (!url.length) {
            return;
        }
        switch ([self httpType]) {
            case PYHttpTypePOST:
            {
                [[PYNetworkHelper instance] postWithUrl:url refreshCache:true params:self.requestParam success:self.completeBlock fail:self.errorBlock modelName:[self modelName]];
                break;
            }
            case PYHttpTypeGET:
            {
                [[PYNetworkHelper instance] getWithUrl:url refreshCache:true params:self.requestParam success:self.completeBlock fail:self.errorBlock modelName:[self modelName]];
                break;
            }
            default:
                break;
        }
    };
    requestBlock();
}

- (NSMutableDictionary *)requestParam
{
    if (!_requestParam) {
        _requestParam = [NSMutableDictionary dictionary];
    }
    return _requestParam;
}

@end
