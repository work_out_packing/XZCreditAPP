//
//  main.m
//  XZCreditAPP
//
//  Created by yunhe.lin on 2017/6/12.
//  Copyright © 2017年 XZDMW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
